FROM alpine:3.17

# Install requirements
RUN apk add --no-cache \
    autoconf \
    automake \
    bash \
    binutils \
    bison \
    bzip2 \
    ccache \
    cmake \
    flex \
    g++ \
    gdk-pixbuf \
    gettext \
    git \
    gperf \
    intltool \
    libtool \
    linux-headers \
    lzip \
    make \
    openssl \
    openssl-dev \
    p7zip \
    patch \
    perl \
    pkgconf \
    python3 \
    py3-mako \
    py3-pip \
    ruby \
    unzip \
    upx \
    wget \
    xz \
    zlib \
    && rm -rf /var/cache/apk/*

RUN python3 -m pip install --no-cache-dir \
    conan \
    meson \
    ninja

# Clone and build mxe
RUN git clone https://github.com/sccoouut/mxe.git /opt/mxe \
    && make --directory=/opt/mxe --jobs=$(nproc --all) JOBS=4 \
    MXE_TARGETS=i686-w64-mingw32.static.posix \
    MXE_PLUGIN_DIRS=plugins/gcc13 \
    boost ccache cmake gcc libzip lua meson openssl pkgconf

# Add mxe to the path
ENV PATH="${PATH}:/opt/mxe/usr/bin"

# Initialize conan
RUN conan profile detect
